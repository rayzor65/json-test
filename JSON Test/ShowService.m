//
//  ShowService.m
//  JSON Test
//
//  Created by raymond ho on 16/11/2013.
//  Copyright (c) 2013 raymond ho. All rights reserved.
//

#import "ShowService.h"

@implementation ShowService

- (void)getShows:(int)page
{
    // No point fetching if we are beyond the page count
    if (page >= _pageCount && _pageCount > 0) {
        NSLog(@"There are no more records");
        return;
    }
    
    NSString *apiUrlStr = [NSString stringWithFormat:@"http://www.whatsbeef.net/wabz/guide.php?start=%d", page];
    NSURL *url = [[NSURL alloc] initWithString:apiUrlStr];

    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {

        if ( ! error) {
            NSError *createError = nil;
            NSMutableArray *fetchedShows = [self createShows:data error:&createError];
            if (fetchedShows) {
                [self postNotificationWithShows:fetchedShows];
            } else {
                NSLog(@"There was an error creating the shows");
            }
        } else {
            NSLog(@"There was an error fetching the shows");
        }
    }];
}

- (NSMutableArray*)createShows:(NSData *)data error:(NSError **)error
{
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil && parsedObject != nil) {
        *error = localError;
        return nil;
    }
    
    NSMutableArray *shows = [[NSMutableArray alloc] init];
    NSArray *results = [parsedObject valueForKey:@"results"];
    int pageCount = [[parsedObject valueForKey:@"count"] intValue];
    _pageCount = pageCount;
    
    for (NSDictionary *dict in results) {
        Show *newShow = [[Show alloc] init];
        newShow.name = [dict valueForKey:@"name"];
        newShow.channel = [dict valueForKey:@"channel"];
        newShow.startTime = [dict valueForKey:@"start_time"];
        newShow.endTime = [dict valueForKey:@"end_time"];
        newShow.rating = [dict valueForKey:@"rating"];
        
        [shows addObject:newShow];
    }
    
    return shows;
}

- (void)postNotificationWithShows:(NSMutableArray*)shows
{
    NSString *notificationName = @"NewShowsNotification";
    NSString *key = @"Shows";
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:shows forKey:key];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
}

@end

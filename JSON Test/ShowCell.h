//
//  ShowCell.h
//  JSON Test
//
//  Created by raymond ho on 16/11/2013.
//  Copyright (c) 2013 raymond ho. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *channelLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;

@end

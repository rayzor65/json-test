//
//  ShowCell.m
//  JSON Test
//
//  Created by raymond ho on 16/11/2013.
//  Copyright (c) 2013 raymond ho. All rights reserved.
//

#import "ShowCell.h"

@implementation ShowCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

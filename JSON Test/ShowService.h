//
//  ShowService.h
//  JSON Test
//
//  Created by raymond ho on 16/11/2013.
//  Copyright (c) 2013 raymond ho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Show.h"

@interface ShowService : NSObject

@property (strong, nonatomic) NSArray *shows;
@property int pageCount;
- (void)getShows:(int)page;

@end

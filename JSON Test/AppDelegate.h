//
//  AppDelegate.h
//  JSON Test
//
//  Created by raymond ho on 16/11/2013.
//  Copyright (c) 2013 raymond ho. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

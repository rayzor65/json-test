//
//  ShowsViewController.h
//  JSON Test
//
//  Created by raymond ho on 16/11/2013.
//  Copyright (c) 2013 raymond ho. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowsViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *shows;
@property (weak, nonatomic) IBOutlet UIButton *scrollButton;
@property int pageNumber;

@end

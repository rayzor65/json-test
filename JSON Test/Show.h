//
//  Show.h
//  JSON Test
//
//  Created by raymond ho on 16/11/2013.
//  Copyright (c) 2013 raymond ho. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Show : NSObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * channel;
@property (nonatomic, retain) NSString * startTime;
@property (nonatomic, retain) NSString * endTime;
@property (nonatomic, retain) NSString * rating;

@end

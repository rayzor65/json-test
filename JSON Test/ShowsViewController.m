//
//  ShowsViewController.m
//  JSON Test
//
//  Created by raymond ho on 16/11/2013.
//  Copyright (c) 2013 raymond ho. All rights reserved.
//

#import "ShowsViewController.h"
#import "ShowService.h"
#import "ShowCell.h"

@interface ShowsViewController ()

@end

@implementation ShowsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //Setup notfications
    NSString *newShowsNotification = @"NewShowsNotification";
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(setShows:)
     name:newShowsNotification
     object:nil];
    
    _pageNumber = 0;
    ShowService *showService = [[ShowService alloc] init];
    [showService getShows:_pageNumber];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_shows count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ShowCell";
    ShowCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    // Configure cell
    Show *show = [_shows objectAtIndex:indexPath.item];
    cell.nameLabel.text = show.name;
    cell.channelLabel.text = show.channel;
    cell.ratingLabel.text = show.rating;
    cell.startTimeLabel.text = show.startTime;
    cell.endTimeLabel.text = show.endTime;
    
    return cell;
}

- (void)setShows:(NSNotification *)notification
{
    NSString *key = @"Shows";
    NSDictionary *dictionary = [notification userInfo];
    NSMutableArray *shows = [dictionary valueForKey:key];
    int originalShowCount = [_shows count];
    
    if (_shows) {
        [_shows addObjectsFromArray:shows];
         NSLog(@"Add shows %@", shows);
    } else {
        _shows = shows;
        NSLog(@"Start Shows %@", _shows);
    }
    
    if (originalShowCount == [_shows count]) {
        NSLog(@"Appear to have connections errors, no results fetched");
    } else {
        // Keep track of the page we are up to
        _pageNumber++;
    }
    
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

- (IBAction)buttonClicked:(id)sender
{
    CGPoint bottomOffset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height);
    [self.tableView setContentOffset:bottomOffset animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
//    NSLog(@"%f", aScrollView.contentSize.height);
//    NSLog(@"%f", aScrollView.contentOffset.y);
    
    float bottomEdge = aScrollView.contentOffset.y + aScrollView.frame.size.height;
    if (bottomEdge == aScrollView.contentSize.height) {
        //NSLog(@"Scrolled bottom, load more data");
        ShowService *showService = [[ShowService alloc] init];
        [showService getShows:_pageNumber + 1];
    }
}

@end
